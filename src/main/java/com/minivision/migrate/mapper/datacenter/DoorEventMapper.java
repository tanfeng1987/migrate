package com.minivision.migrate.mapper.datacenter;


import com.minivision.migrate.common.IMapper;
import com.minivision.migrate.entity.DoorEvent;

public interface DoorEventMapper extends IMapper<DoorEvent> {
}