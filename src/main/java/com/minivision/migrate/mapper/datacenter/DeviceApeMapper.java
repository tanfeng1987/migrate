package com.minivision.migrate.mapper.datacenter;


import com.minivision.migrate.common.IMapper;
import com.minivision.migrate.entity.DeviceApe;

public interface DeviceApeMapper extends IMapper<DeviceApe> {
}