package com.minivision.migrate.mapper.datacenter;


import com.minivision.migrate.common.IMapper;
import com.minivision.migrate.entity.VillageInfo;

public interface VillageInfoMapper extends IMapper<VillageInfo> {
}