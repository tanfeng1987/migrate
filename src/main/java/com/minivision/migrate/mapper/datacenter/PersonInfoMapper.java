package com.minivision.migrate.mapper.datacenter;


import com.minivision.migrate.common.IMapper;
import com.minivision.migrate.entity.PersonInfo;

public interface PersonInfoMapper extends IMapper<PersonInfo> {
}