package com.minivision.migrate.mapper.datacenter;


import com.minivision.migrate.common.IMapper;
import com.minivision.migrate.entity.HouseInfo;

public interface HouseInfoMapper extends IMapper<HouseInfo> {

    HouseInfo getHouseInfoByOrgId(String orgId);
}