package com.minivision.migrate.mapper.salmon;

import com.minivision.migrate.common.IMapper;
import com.minivision.migrate.entity.Area;

public interface AreaMapper extends IMapper<Area> {
}