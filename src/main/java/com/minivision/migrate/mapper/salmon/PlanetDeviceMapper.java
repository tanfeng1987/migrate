package com.minivision.migrate.mapper.salmon;

import com.minivision.migrate.common.IMapper;
import com.minivision.migrate.dto.PlanetDeviceDTO;
import com.minivision.migrate.entity.PlanetDevice;

import java.util.List;

public interface PlanetDeviceMapper extends IMapper<PlanetDevice> {


    /**
     * 根据客户id查询设备信息
     * @param custId
     * @return
     */
    List<PlanetDeviceDTO> queryPlanetDeviceByCustId(String custId);

    /**
     * 查询设备的进出类型
     * @param deviceSn
     * @return
     */
    String getInOutTypeBySn(String deviceSn);

    /**
     * 查询设备状态
     * @param deviceSn
     * @return
     */
    PlanetDeviceDTO getDeviceStatusBySn(String deviceSn);

}