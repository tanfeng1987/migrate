package com.minivision.migrate.mapper.person;

import com.minivision.migrate.common.IMapper;
import com.minivision.migrate.dto.PlanetPersonDTO;
import com.minivision.migrate.entity.PlanetPerson;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PlanetPersonMapper extends IMapper<PlanetPerson> {


    List<PlanetPersonDTO> queryPlanetPersonByOrgIds(@Param("orgIdList") List<Integer> orgIdList);
}