package com.minivision.migrate.mapper.planet;

import com.minivision.migrate.common.IMapper;
import com.minivision.migrate.entity.Org;

public interface OrgMapper extends IMapper<Org> {

    /**
     * 根据小区id查询客户id
     * @param communityId
     * @return
     */
    String getCustIdByCommunityId(Integer communityId);
}