package com.minivision.migrate.mapper.planet;

import com.minivision.migrate.common.IMapper;
import com.minivision.migrate.entity.SysAppConfig;

public interface SysAppConfigMapper extends IMapper<SysAppConfig> {
}