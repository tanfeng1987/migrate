/****************************************************************************************
 南京小视科技有限公司
 ****************************************************************************************/
package com.minivision.migrate.service.impl;

import com.minivision.migrate.entity.DeviceApe;
import com.minivision.migrate.entity.DoorEvent;
import com.minivision.migrate.entity.HouseInfo;
import com.minivision.migrate.entity.PersonInfo;
import com.minivision.migrate.entity.VillageInfo;
import com.minivision.migrate.mapper.datacenter.DeviceApeMapper;
import com.minivision.migrate.mapper.datacenter.DoorEventMapper;
import com.minivision.migrate.mapper.datacenter.HouseInfoMapper;
import com.minivision.migrate.mapper.datacenter.PersonInfoMapper;
import com.minivision.migrate.mapper.datacenter.VillageInfoMapper;
import com.minivision.migrate.service.DataPlanetService;
import com.minivision.migrate.service.MigrateService;
import com.minivision.migrate.service.PlanetService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <Description><br>
 *
 * @author luyaoqiang<br>
 * @version 1.2<br>
 * @taskId <br>
 * @CreateDate 2020/9/29 <br>
 */
@Slf4j
@Service
public class DataPlanetServiceImpl implements DataPlanetService {

    @Autowired
    private DeviceApeMapper deviceApeMapper;
    @Autowired
    private PersonInfoMapper personInfoMapper;
    @Autowired
    private VillageInfoMapper villageInfoMapper;
    @Autowired
    private HouseInfoMapper houseInfoMapper;
    @Autowired
    private DoorEventMapper doorEventMapper;

    void insertDeviceApe() {

        DeviceApe deviceApe = new DeviceApe();
        deviceApe.setSbmc("设备名称");
        deviceApe.setSblx("003"); // 设备类型
        deviceApe.setXqxxbz(1); // 小区信息标识
        deviceApe.setXqmh("小区门号");
        deviceApe.setLdh("楼栋信息");
        deviceApe.setDyh("单元信息");
        deviceApe.setIpdz("IP地址");
        deviceApe.setMacdz("MAC地址");
        deviceApe.setZbtx("坐标体系");
        deviceApe.setDqjd("地球经度");
        deviceApe.setDqwd("地球纬度");
        deviceApe.setSfzxPdbz("是否在线_判断标识");
        deviceApe.setCjsj(new Date()); //  设备信息采集时间，统一格式为yyyy-mm-dd hh24：mi：ss
        deviceApe.setWzlx("选取如下取值代码：\n" +
                "0-其他\n" +
                "1-小区出入口门禁\n" +
                "2-单元出入口门禁");
        deviceApe.setXjfx("行进方向");

        deviceApeMapper.insertSelective(deviceApe);
    }

    void insertVillageInfo() {
        VillageInfo villageInfo = new VillageInfo();

        villageInfo.setJlxxqmc("小区名称");
        villageInfo.setHjdzXzqhdm(0); // 行政区划代码,6位，代表小区所在省市区
        villageInfo.setGxsj(new Date()); // 数据更新时间
        villageInfo.setXqlx(1); // 小区类型
        villageInfo.setDzmc("地址名称");
        villageInfo.setXqldSl(1); // 小区楼栋_数量
        villageInfo.setShxqldh("所含楼栋编号");
        villageInfo.setXqqyRq(new Date()); //  小区启用_日期
        villageInfo.setXqwygsDwmc("物业公司_单位名称");
        villageInfo.setXqwygsXm("物业公司_联系人姓名");
        villageInfo.setXqwygsLxdh("物业公司_联系电话");

        villageInfoMapper.insertSelective(villageInfo);
    }

    void insertHouseInfo() {
        HouseInfo houseInfo = new HouseInfo();
        houseInfo.setXqxxbz(1); //小区信息标识
        houseInfo.setLdxxbz("楼栋名称");
        houseInfo.setDyxxbz("单元名称");
        houseInfo.setFjxxbz("房间号");
        houseInfo.setGxsj(new Date()); // 数据更新时间
        houseInfo.setLch("楼层号");


        houseInfoMapper.insertSelective(houseInfo);
    }

    void insertPersonInfo() {
        PersonInfo personInfo = new PersonInfo();

        personInfo.setXm("姓 名");
        personInfo.setKmsbbz("开门识别标识");
        personInfo.setKmfsdm("开门方式代码");
        personInfo.setXqxxbz(1);// 小区信息标识
        personInfo.setFwxxbz(1); // 房屋信息标识

        personInfo.setGxsj(new Date()); // 数据更新时间
        personInfo.setLxdh("联系电话");
        personInfo.setXbdm(1); // 性别代码
        personInfo.setXp("登记相片");

        personInfoMapper.insertSelective(personInfo);
    }

    void insertDoorEvent() {
        DoorEvent doorEvent = new DoorEvent();
        doorEvent.setCjsbxxbz(1); // 采集设备信息标识
        doorEvent.setXqxxbz(1); // 小区信息标识
        doorEvent.setFwxxbz(1); // 房屋信息标识
        doorEvent.setRyxxbz(1); // 人员信息标识

        doorEvent.setKmsbbz("开门设别标识");
        doorEvent.setKmfsdm("开门方式代码");

        doorEvent.setCjsj(new Date()); //  采集时间
        doorEvent.setTp("开门照片");

        doorEvent.setXm("姓名");

        doorEventMapper.insertSelective(doorEvent);
    }

    @Autowired
    private MigrateService migrateService;

    @Autowired
    private PlanetService planetService;

    /**
     * 清洗星环云数据到数据中心
     *
     *  客户账户登录后，在组织管理中的小区id（也就是客户账户登录后第一级组织的id）
     */
    @Override
    public void sync() {
        log.info("sync开始执行");
        List<Integer> communityIdList = new ArrayList<>();

        // 菊花里小区的组织id
        communityIdList.add(7872);

        for (Integer communityId : communityIdList) {
            // 先处理小区和房屋信息
            Integer villageId = migrateService.migrateVillageAndHouse(communityId);

            // 处理设备信息
            planetService.dealDevice(String.valueOf(communityId));

            // 再处理人员信息
            planetService.dealPlanetPersonByCommunityId(String.valueOf(communityId));

            // 处理识别记录
            migrateService.migratePadRecord(villageId);
        }

        log.info("sync结束执行");
    }
}
