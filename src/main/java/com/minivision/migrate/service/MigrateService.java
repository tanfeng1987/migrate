package com.minivision.migrate.service;

import java.util.List;

public interface MigrateService {
    /**
     * 同步小区和房屋信息
     * @param orgId
     * @return
     */
    Integer migrateVillageAndHouse(int orgId);

    /**
     * 同步pad识别记录
     * @param villageId
     */
    void migratePadRecord(int villageId);

    /**
     * 查询小区下的所有组织id
     * @param orgId
     * @return
     */
    List<Integer> getAllSubOrgIds(int orgId);
}
