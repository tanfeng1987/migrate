/****************************************************************************************
 南京小视科技有限公司
 ****************************************************************************************/
package com.minivision.migrate.service;

import java.util.List;

/**
 * <Description><br>
 *
 * @author luyaoqiang<br>
 * @version 1.2<br>
 * @taskId <br>
 * @CreateDate 2020/9/29 <br>
 */
public interface DataPlanetService {

    /**
     * 清洗星环云数据到数据中心
     *  客户账户登录后，在组织管理中的小区id（也就是客户账户登录后第一级组织的id）
     */
    void sync();

}
