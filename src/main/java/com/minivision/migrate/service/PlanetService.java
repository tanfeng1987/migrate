/****************************************************************************************
 南京小视科技有限公司
 ****************************************************************************************/
package com.minivision.migrate.service;

import com.minivision.migrate.dto.PlanetPersonDTO;

import java.util.List;

/**
 * <Description><br>
 *
 * @author luyaoqiang<br>
 * @version 1.2<br>
 * @taskId <br>
 * @CreateDate 2020/9/29 <br>
 */
public interface PlanetService {

    /**
     * 处理人员
     * @param communityId
     */
    void dealPlanetPersonByCommunityId(String communityId);

    /**
     * 处理设备
     * @param communityId
     */
    void dealDevice(String communityId);
}
