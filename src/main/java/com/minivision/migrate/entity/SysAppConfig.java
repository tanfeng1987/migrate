package com.minivision.migrate.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "t_sys_app_config")
public class SysAppConfig {
    @Id
    private String id;

    /**
     * 应用名称
     */
    @Column(name = "app_name")
    private String appName;

    /**
     * 应用logo
     */
    @Column(name = "app_logo")
    private String appLogo;

    /**
     * 跳转地址
     */
    @Column(name = "app_url")
    private String appUrl;

    /**
     * 应用类型 1安防 2零售
     */
    @Column(name = "app_type")
    private String appType;

    /**
     * 应用状态 0禁用 1启用
     */
    @Column(name = "app_status")
    private Boolean appStatus;

    /**
     * 应用所属分组 0-基础应用 1-第三方应用
     */
    @Column(name = "app_group")
    private String appGroup;

    private String creator;

    /**
     * 创建人id
     */
    @Column(name = "creator_id")
    private String creatorId;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新人
     */
    private String modifier;

    /**
     * 更新人id
     */
    @Column(name = "modifier_id")
    private String modifierId;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 1表示已经删除
     */
    @Column(name = "is_delete")
    private String isDelete;

    /**
     * 应用描述
     */
    private String remark;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取应用名称
     *
     * @return app_name - 应用名称
     */
    public String getAppName() {
        return appName;
    }

    /**
     * 设置应用名称
     *
     * @param appName 应用名称
     */
    public void setAppName(String appName) {
        this.appName = appName;
    }

    /**
     * 获取应用logo
     *
     * @return app_logo - 应用logo
     */
    public String getAppLogo() {
        return appLogo;
    }

    /**
     * 设置应用logo
     *
     * @param appLogo 应用logo
     */
    public void setAppLogo(String appLogo) {
        this.appLogo = appLogo;
    }

    /**
     * 获取跳转地址
     *
     * @return app_url - 跳转地址
     */
    public String getAppUrl() {
        return appUrl;
    }

    /**
     * 设置跳转地址
     *
     * @param appUrl 跳转地址
     */
    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }

    /**
     * 获取应用类型 1安防 2零售
     *
     * @return app_type - 应用类型 1安防 2零售
     */
    public String getAppType() {
        return appType;
    }

    /**
     * 设置应用类型 1安防 2零售
     *
     * @param appType 应用类型 1安防 2零售
     */
    public void setAppType(String appType) {
        this.appType = appType;
    }

    /**
     * 获取应用状态 0禁用 1启用
     *
     * @return app_status - 应用状态 0禁用 1启用
     */
    public Boolean getAppStatus() {
        return appStatus;
    }

    /**
     * 设置应用状态 0禁用 1启用
     *
     * @param appStatus 应用状态 0禁用 1启用
     */
    public void setAppStatus(Boolean appStatus) {
        this.appStatus = appStatus;
    }

    /**
     * 获取应用所属分组 0-基础应用 1-第三方应用
     *
     * @return app_group - 应用所属分组 0-基础应用 1-第三方应用
     */
    public String getAppGroup() {
        return appGroup;
    }

    /**
     * 设置应用所属分组 0-基础应用 1-第三方应用
     *
     * @param appGroup 应用所属分组 0-基础应用 1-第三方应用
     */
    public void setAppGroup(String appGroup) {
        this.appGroup = appGroup;
    }

    /**
     * @return creator
     */
    public String getCreator() {
        return creator;
    }

    /**
     * @param creator
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * 获取创建人id
     *
     * @return creator_id - 创建人id
     */
    public String getCreatorId() {
        return creatorId;
    }

    /**
     * 设置创建人id
     *
     * @param creatorId 创建人id
     */
    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取更新人
     *
     * @return modifier - 更新人
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * 设置更新人
     *
     * @param modifier 更新人
     */
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    /**
     * 获取更新人id
     *
     * @return modifier_id - 更新人id
     */
    public String getModifierId() {
        return modifierId;
    }

    /**
     * 设置更新人id
     *
     * @param modifierId 更新人id
     */
    public void setModifierId(String modifierId) {
        this.modifierId = modifierId;
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取1表示已经删除
     *
     * @return is_delete - 1表示已经删除
     */
    public String getIsDelete() {
        return isDelete;
    }

    /**
     * 设置1表示已经删除
     *
     * @param isDelete 1表示已经删除
     */
    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * 获取应用描述
     *
     * @return remark - 应用描述
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置应用描述
     *
     * @param remark 应用描述
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
}