package com.minivision.migrate.entity;

import lombok.Data;

import java.util.Date;
import javax.persistence.*;

@Data
@Table(name = "t_device_info")
public class PlanetDevice {
    /**
     * 唯一标识
     */
    @Id
    private String id;

    /**
     * 应用key
     */
    @Column(name = "app_key")
    private String appKey;

    /**
     * 设备名称
     */
    @Column(name = "device_name")
    private String deviceName;

    /**
     * 设备编码
     */
    @Column(name = "device_code")
    private String deviceCode;

    /**
     * 设备描述
     */
    @Column(name = "device_desc")
    private String deviceDesc;

    /**
     * 设备类型
     */
    @Column(name = "device_type")
    private String deviceType;

    /**
     * 当前版本
     */
    private String version;

    @Column(name = "device_ipv4")
    private String deviceIpv4;

    @Column(name = "device_mac")
    private String deviceMac;

    /**
     * 上级设备
     */
    @Column(name = "p_id")
    private String pId;

    /**
     * 是否删除0未删除1已删除
     */
    @Column(name = "is_delete")
    private String isDelete;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    private String creator;

    /**
     * 创建人ID
     */
    @Column(name = "creator_id")
    private String creatorId;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    private String modifier;

    /**
     * 修改人ID
     */
    @Column(name = "modifier_id")
    private String modifierId;

    /**
     * 上级设备编码
     */
    @Column(name = "p_code")
    private String pCode;

    /**
     * 设备型号
     */
    private String specification;

}