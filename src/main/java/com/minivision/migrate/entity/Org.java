package com.minivision.migrate.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "tc_rbac_org")
public class Org {
    /**
     * 自增id
     */
    @Id
    private Integer id;

    /**
     * 父组织id
     */
    @Column(name = "parent_id")
    private Integer parentId;

    /**
     * 组织名称
     */
    private String name;

    /**
     * 组织层级
     */
    @Column(name = "level_num")
    private Byte levelNum;

    private String description;

    /**
     * 组织地址
     */
    private String addr;

    /**
     * 组织代码
     */
    @Column(name = "org_code")
    private String orgCode;

    /**
     * 内线电话
     */
    @Column(name = "inter_phone")
    private String interPhone;

    /**
     * 外线电话
     */
    @Column(name = "out_phone")
    private String outPhone;

    /**
     * 机构类型
     */
    @Column(name = "org_type")
    private String orgType;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 创建者
     */
    private String creator;

    /**
     * 是否已经删除，0表示未删除 1表示已经删除
     */
    @Column(name = "is_delete")
    private String isDelete;

    /**
     * 修改者id
     */
    @Column(name = "modifier_id")
    private String modifierId;

    /**
     * 修改者名称
     */
    private String modifier;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 创建者id
     */
    @Column(name = "creator_id")
    private String creatorId;

    /**
     * 0表示禁用，1表示启用
     */
    private Byte status;

    @Column(name = "org_wrap_id")
    private String orgWrapId;

    /**
     * 获取自增id
     *
     * @return id - 自增id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置自增id
     *
     * @param id 自增id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取父组织id
     *
     * @return parent_id - 父组织id
     */
    public Integer getParentId() {
        return parentId;
    }

    /**
     * 设置父组织id
     *
     * @param parentId 父组织id
     */
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    /**
     * 获取组织名称
     *
     * @return name - 组织名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置组织名称
     *
     * @param name 组织名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取组织层级
     *
     * @return level_num - 组织层级
     */
    public Byte getLevelNum() {
        return levelNum;
    }

    /**
     * 设置组织层级
     *
     * @param levelNum 组织层级
     */
    public void setLevelNum(Byte levelNum) {
        this.levelNum = levelNum;
    }

    /**
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 获取组织地址
     *
     * @return addr - 组织地址
     */
    public String getAddr() {
        return addr;
    }

    /**
     * 设置组织地址
     *
     * @param addr 组织地址
     */
    public void setAddr(String addr) {
        this.addr = addr;
    }

    /**
     * 获取组织代码
     *
     * @return org_code - 组织代码
     */
    public String getOrgCode() {
        return orgCode;
    }

    /**
     * 设置组织代码
     *
     * @param orgCode 组织代码
     */
    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    /**
     * 获取内线电话
     *
     * @return inter_phone - 内线电话
     */
    public String getInterPhone() {
        return interPhone;
    }

    /**
     * 设置内线电话
     *
     * @param interPhone 内线电话
     */
    public void setInterPhone(String interPhone) {
        this.interPhone = interPhone;
    }

    /**
     * 获取外线电话
     *
     * @return out_phone - 外线电话
     */
    public String getOutPhone() {
        return outPhone;
    }

    /**
     * 设置外线电话
     *
     * @param outPhone 外线电话
     */
    public void setOutPhone(String outPhone) {
        this.outPhone = outPhone;
    }

    /**
     * 获取机构类型
     *
     * @return org_type - 机构类型
     */
    public String getOrgType() {
        return orgType;
    }

    /**
     * 设置机构类型
     *
     * @param orgType 机构类型
     */
    public void setOrgType(String orgType) {
        this.orgType = orgType;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取创建者
     *
     * @return creator - 创建者
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 设置创建者
     *
     * @param creator 创建者
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * 获取是否已经删除，0表示未删除 1表示已经删除
     *
     * @return is_delete - 是否已经删除，0表示未删除 1表示已经删除
     */
    public String getIsDelete() {
        return isDelete;
    }

    /**
     * 设置是否已经删除，0表示未删除 1表示已经删除
     *
     * @param isDelete 是否已经删除，0表示未删除 1表示已经删除
     */
    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * 获取修改者id
     *
     * @return modifier_id - 修改者id
     */
    public String getModifierId() {
        return modifierId;
    }

    /**
     * 设置修改者id
     *
     * @param modifierId 修改者id
     */
    public void setModifierId(String modifierId) {
        this.modifierId = modifierId;
    }

    /**
     * 获取修改者名称
     *
     * @return modifier - 修改者名称
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * 设置修改者名称
     *
     * @param modifier 修改者名称
     */
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取创建者id
     *
     * @return creator_id - 创建者id
     */
    public String getCreatorId() {
        return creatorId;
    }

    /**
     * 设置创建者id
     *
     * @param creatorId 创建者id
     */
    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    /**
     * 获取0表示禁用，1表示启用
     *
     * @return status - 0表示禁用，1表示启用
     */
    public Byte getStatus() {
        return status;
    }

    /**
     * 设置0表示禁用，1表示启用
     *
     * @param status 0表示禁用，1表示启用
     */
    public void setStatus(Byte status) {
        this.status = status;
    }

    /**
     * @return org_wrap_id
     */
    public String getOrgWrapId() {
        return orgWrapId;
    }

    /**
     * @param orgWrapId
     */
    public void setOrgWrapId(String orgWrapId) {
        this.orgWrapId = orgWrapId;
    }
}