package com.minivision.migrate.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "zhxq_villageinfo")
@Data
public class VillageInfo {
    /**
     * 小区信息标识
     */
    @Id
    @Column(name = "XQXXBZ")
    private Integer xqxxbz;

    /**
     * 小区名称
     */
    @Column(name = "JLXXQMC")
    private String jlxxqmc;

    /**
     * 行政区划代码
     */
    @Column(name = "HJDZ_XZQHDM")
    private Integer hjdzXzqhdm;

    /**
     * 数据更新时间
     */
    @Column(name = "GXSJ")
    private Date gxsj;

    /**
     * 数据来源
     */
    @Column(name = "SJLY")
    private String sjly;

    /**
     * 小区类型
     * 1-封闭式
     * 2-半封闭式
     * 3-开放式
     * 9-其他
     */
    @Column(name = "XQLX")
    private Integer xqlx;

    /**
     * 地址编码
     */
    @Column(name = "DZBM")
    private String dzbm;

    /**
     * 地址名称
     */
    @Column(name = "DZMC")
    private String dzmc;

    /**
     * 小区楼栋_数量
     */
    @Column(name = "XQLD_SL")
    private Integer xqldSl;

    /**
     * 所含楼栋编号
     */
    @Column(name = "SHXQLDH")
    private String shxqldh;

    /**
     * 小区启用_日期
     */
    @Column(name = "XQQY_RQ")
    private Date xqqyRq;

    /**
     * 物业公司_单位名称
     */
    @Column(name = "XQWYGS_DWMC")
    private String xqwygsDwmc;

    /**
     * 物业公司_联系人姓名
     */
    @Column(name = "XQWYGS_XM")
    private String xqwygsXm;

    /**
     * 物业公司_联系电话
     */
    @Column(name = "XQWYGS_LXDH")
    private String xqwygsLxdh;

    /**
     * 小区出入口_数量
     */
    @Column(name = "XQCRK_SL")
    private Integer xqcrkSl;

    /**
     * 星环云组织id
     */
    @Column(name = "planet_org_id")
    private Integer planetOrgId;

    /**
     * 小区图片
     */
    @Column(name = "TP")
    private String tp;
}