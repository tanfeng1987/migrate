package com.minivision.migrate.entity;

import lombok.Data;

import java.util.Date;
import javax.persistence.*;

@Data
@Table(name = "t_p_person")
public class PlanetPerson {
    /**
     * id
     */
    @Id
    private String id;

    /**
     * 所属客户id(企业id)
     */
    @Column(name = "company_id")
    private String companyId;

    /**
     * 名称
     */
    private String name;

    /**
     * 人员类型(没指定时为员工)
     */
    private Byte type;

    /**
     * 工号/编号 可为空
     */
    private String no;

    /**
     * 性别
     */
    private Byte sex;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 年龄
     */
    private Byte age;

    /**
     * 身份证id
     */
    @Column(name = "card_no")
    private String cardNo;

    /**
     * 展示照片(用于展示)
     */
    @Column(name = "show_photo")
    private String showPhoto;

    /**
     * 是否删除
     */
    @Column(name = "is_delete")
    private Boolean isDelete;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 备注
     */
    private String remark;

    /**
     * 请求来源
     */
    @Column(name = "app_key")
    private String appKey;

    /**
     * 外部人员id
     */
    @Column(name = "outer_person_id")
    private String outerPersonId;

    /**
     * 创建者
     */
    private String creator;

    @Column(name = "creator_id")
    private String creatorId;

    private String modifier;

    @Column(name = "modifier_id")
    private String modifierId;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 审核状态，1表示正常，0表示未审核，-1表示审核不通过
     */
    private Byte status;

    /**
     * 审核描述
     */
    @Column(name = "audit_remark")
    private String auditRemark;

    /**
     * IC卡号
     */
    @Column(name = "ic_card")
    private String icCard;


}