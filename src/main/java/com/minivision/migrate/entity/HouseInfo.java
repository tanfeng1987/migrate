package com.minivision.migrate.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "zhxq_houseinfo")
@Data
public class HouseInfo {
    /**
     * 房屋信息标识
     */
    @Id
    @Column(name = "FWXXBZ")
    private Integer fwxxbz;

    /**
     * 小区信息标识
     */
    @Column(name = "XQXXBZ")
    private Integer xqxxbz;

    /**
     * 楼栋名称
     */
    @Column(name = "LDXXBZ")
    private String ldxxbz;

    /**
     * 单元名称
     */
    @Column(name = "DYXXBZ")
    private String dyxxbz;

    /**
     * 房间号
     */
    @Column(name = "FJXXBZ")
    private String fjxxbz;

    /**
     * 数据更新时间
     */
    @Column(name = "GXSJ")
    private Date gxsj;

    /**
     * 数据来源
     */
    @Column(name = "SJLY")
    private String sjly;

    /**
     * 楼层号
     */
    @Column(name = "LCH")
    private String lch;

    /**
     * 房主_姓名
     */
    @Column(name = "FZ_XM")
    private String fzXm;

    /**
     * 房主_公民身份号码
     */
    @Column(name = "FZ_GMSFZHM")
    private String fzGmsfzhm;

    /**
     * 房主_联系电话
     */
    @Column(name = "FZ_LXDH")
    private String fzLxdh;

    /**
     * 居住_人数
     */
    @Column(name = "JZ_RS")
    private Integer jzRs;

    /**
     * 房屋间数
     */
    @Column(name = "FWJS")
    private Integer fwjs;

    /**
     * 建筑_面积(平方米)
     */
    @Column(name = "JZ_MJPFM")
    private String jzMjpfm;

    /**
     * 房屋产权证号
     */
    @Column(name = "FWCQZH")
    private String fwcqzh;

    /**
     * 是否为重点房屋_判断标识
     */
    @Column(name = "SFWZDFW_PDBZ")
    private String sfwzdfwPdbz;

    /**
     * 居住情况类型
0-房主
1-租客
2-空置等
     */
    @Column(name = "JZQKLX")
    private Integer jzqklx;

    /**
     * 房屋类别代码
0-公寓楼
1-单元楼
2-别墅等
     */
    @Column(name = "FWLBDM")
    private Integer fwlbdm;

    /**
     * 房屋所属单位_法定代表人姓名
     */
    @Column(name = "FWSSDW_FDDBRXM")
    private String fwssdwFddbrxm;

    /**
     * 房屋所属单位_统一社会信用代码
     */
    @Column(name = "FWSSDW_TYSHXYDM")
    private String fwssdwTyshxydm;

    /**
     * 源表房屋关联ID
     */
    @Column(name = "ROOM_ID")
    private String roomId;

    /**
     * 星环云组织id
     */
    @Column(name = "planet_org_id")
    private Integer planetOrgId;
}