package com.minivision.migrate.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "zhxq_personinfo")
@Data
public class PersonInfo {
    /**
     * 人员信息标识
     */
    @Id
    @Column(name = "RYXXBZ")
    private Integer ryxxbz;

    /**
     * 姓名
     */
    @Column(name = "XM")
    private String xm;

    /**
     * 开门识别标识
     */
    @Column(name = "KMSBBZ")
    private String kmsbbz;

    /**
     * 开门方式代码
     * 01-刷卡
     * 02-手机
     * 03-密码
     * 04-指纹
     * 05-二维码
     * 06-虹膜
     * 07-掌纹
     * 08-指静脉
     * 09-人脸
     * 10-声纹
     * 99-其他
     */
    @Column(name = "KMFSDM")
    private String kmfsdm;

    /**
     * 小区信息标识
     */
    @Column(name = "XQXXBZ")
    private Integer xqxxbz;

    /**
     * 房屋信息标识
     */
    @Column(name = "FWXXBZ")
    private Integer fwxxbz;

    /**
     * 常用证件代码
     * 111-身份证
     * 112-临时居民身份证
     * 113-户口簿
     * 114-中国人民解放军军官证
     * 115-中国人民武装警察部队警官证
     * 116-暂住证
     */
    @Column(name = "CYZJDM")
    private String cyzjdm;

    /**
     * 证件号码
     */
    @Column(name = "ZJHM")
    private String zjhm;

    /**
     * 人员登记类型
     * 1-常住人口
     * 2-流动人口
     * 3-境外人口
     * 9-其他
     */
    @Column(name = "RYDJLX")
    private String rydjlx;

    /**
     * 数据更新时间
     */
    @Column(name = "GXSJ")
    private Date gxsj;

    /**
     * 联系电话
     */
    @Column(name = "LXDH")
    private String lxdh;

    /**
     * 数据来源
     */
    @Column(name = "SJLY")
    private String sjly;

    /**
     * 曾用名
     */
    @Column(name = "CYM")
    private String cym;

    /**
     * 性别代码
     * 0-男
     * 1-女
     */
    @Column(name = "XBDM")
    private Integer xbdm;

    /**
     * 名下机动车号牌号码
     */
    @Column(name = "JDCHPHM")
    private String jdchphm;

    /**
     * 入境_日期
     */
    @Column(name = "RJ_RQ")
    private Date rjRq;

    /**
     * 外文姓
     */
    @Column(name = "WWX")
    private String wwx;

    /**
     * 外文名
     */
    @Column(name = "WWM")
    private String wwm;

    /**
     * 星环云人员id
     */
    @Column(name = "planet_person_id")
    private String planetPersonId;

    /**
     * 登记相片
     */
    @Column(name = "XP")
    private String xp;
}