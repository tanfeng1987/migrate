package com.minivision.migrate.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "zhxq_doorevent")
@Data
public class DoorEvent {
    /**
     * 门禁进出记录信息标识
     */
    @Id
    @Column(name = "MJJCJLXXBZ")
    private Integer mjjcjlxxbz;

    /**
     * 采集设备信息标识
     */
    @Column(name = "CJSBXXBZ")
    private Integer cjsbxxbz;

    /**
     * 小区信息标识
     */
    @Column(name = "XQXXBZ")
    private Integer xqxxbz;

    /**
     * 房屋信息标识
     */
    @Column(name = "FWXXBZ")
    private Integer fwxxbz;

    /**
     * 人员信息标识
     */
    @Column(name = "RYXXBZ")
    private Integer ryxxbz;

    /**
     * 开门识别标识
     */
    @Column(name = "KMSBBZ")
    private String kmsbbz;

    /**
     * 开门方式代码
01-刷卡
02-手机
03-密码
04-指纹
05-二维码
06-虹膜
07-掌纹
08-指静脉
09-人脸
10-声纹
99-其他
     */
    @Column(name = "KMFSDM")
    private String kmfsdm;

    /**
     * 采集时间
     */
    @Column(name = "CJSJ")
    private Date cjsj;

    /**
     * 是否通过_判断标识
     */
    @Column(name = "SFTG_PDBZ")
    private String sftgPdbz;

    /**
     * 数据来源
     */
    @Column(name = "SJLY")
    private String sjly;

    /**
     * 姓名
     */
    @Column(name = "XM")
    private String xm;

    /**
     * 常用证件代码
111-身份证
112-临时居民身份证
113-户口簿
114-中国人民解放军军官证
115-中国人民武装警察部队警官证
116-暂住证
     */
    @Column(name = "CYZJDM")
    private String cyzjdm;

    /**
     * 证件号码
     */
    @Column(name = "ZJHM")
    private String zjhm;

    /**
     * 门磁状态
     */
    @Column(name = "MCZT")
    private String mczt;

    /**
     * 开门结果
     */
    @Column(name = "KMJG")
    private String kmjg;

    /**
     * 开门刷卡次数
     */
    @Column(name = "KMSKCS")
    private Integer kmskcs;

    /**
     * 星环云识别记录id
     */
    @Column(name = "planet_record_id")
    private String planetRecordId;

    /**
     * 开门照片
     */
    @Column(name = "TP")
    private String tp;
}