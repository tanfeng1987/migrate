package com.minivision.migrate.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "zhxq_ape")
@Data
public class DeviceApe {
    /**
     * 采集设备信息标识
     */
    @Id
    @Column(name = "CJSBXXBZ")
    private Integer cjsbxxbz;

    /**
     * 设备名称
     */
    @Column(name = "SBMC")
    private String sbmc;

    /**
     * 设备类型
     * 001-人脸摄像机
     * 002-车辆抓拍机
     * 003-门禁主机设备
     * 004-电子巡更设备
     * 005-访客登记设备
     * 006-RFID设备
     * 007-报警主机设备
     * 008-信息发布设备
     * 009-车位状态检测设备
     * 010-Wifi探针设备
     * 011-消防感知设备
     * 999-其他
     */
    @Column(name = "SBLX")
    private String sblx;

    /**
     * 小区信息标识
     */
    @Column(name = "XQXXBZ")
    private Integer xqxxbz;

    /**
     * 小区门号
     */
    @Column(name = "XQMH")
    private String xqmh;

    /**
     * 楼栋信息
     */
    @Column(name = "LDH")
    private String ldh;

    /**
     * 单元信息
     */
    @Column(name = "DYH")
    private String dyh;

    /**
     * 数据来源
     */
    @Column(name = "SJLY")
    private String sjly;

    /**
     * 品牌型号
     */
    @Column(name = "PPXH")
    private String ppxh;

    /**
     * 厂商名称
     */
    @Column(name = "CCMC")
    private String ccmc;

    /**
     * IP地址
     */
    @Column(name = "IPDZ")
    private String ipdz;

    /**
     * MAC地址
     */
    @Column(name = "MACDZ")
    private String macdz;

    /**
     * IPv6地址
     */
    @Column(name = "IPV6DZ")
    private String ipv6dz;

    /**
     * 网络端口号
     */
    @Column(name = "WLDKH")
    private String wldkh;

    /**
     * 坐标体系
     */
    @Column(name = "ZBTX")
    private String zbtx;

    /**
     * 地球经度
     */
    @Column(name = "DQJD")
    private String dqjd;

    /**
     * 地球纬度
     */
    @Column(name = "DQWD")
    private String dqwd;

    /**
     * 是否在线_判断标识
     * 设备是否在线
     * 0-不在线
     * 1-在线
     */
    @Column(name = "SFZX_PDBZ")
    private String sfzxPdbz;

    /**
     * 采集时间
     */
    @Column(name = "CJSJ")
    private Date cjsj;

    /**
     * 最后在线时间
     */
    @Column(name = "ZHZXSJ")
    private Date zhzxsj;

    /**
     * 采集系统信息标识
     */
    @Column(name = "CJXTXXBZ")
    private String cjxtxxbz;

    /**
     * 位置类型
     * 0-其他
     * 1-小区出入口门禁
     * 2-单元出入口门禁
     */
    @Column(name = "WZLX")
    private String wzlx;

    /**
     * 行进方向
     * 1-进
     * 2-出
     * 3-进出
     * 9-其他
     */
    @Column(name = "XJFX")
    private String xjfx;

    /**
     * 星环云设备sn
     */
    @Column(name = "planet_device_sn")
    private String planetDeviceSn;
}