package com.minivision.migrate.config;

import com.alibaba.druid.pool.DruidDataSource;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import tk.mybatis.spring.annotation.MapperScan;

import javax.sql.DataSource;

@Data
@Slf4j
@ConfigurationProperties(prefix="datasource.planet")
@Configuration
@MapperScan(basePackages = {"com.minivision.migrate.mapper.planet"}, sqlSessionFactoryRef = "planetSqlSessionFactory")
public class PlanetDBConfig {

    private String url;

    private String username;

    private String password;

    private String driverClassName;

    @Primary
    @Bean(name="planetDataSource")
    public DataSource planetDataSource() {
        log.info("-------------------- planetDataSource init ---------------------");
        DruidDataSource ds = new DruidDataSource();
        ds.setUrl(url);
        ds.setUsername(username);
        ds.setPassword(password);
        ds.setDriverClassName(driverClassName);
        return ds;
    }

    @Bean
    public SqlSessionFactory planetSqlSessionFactory() throws Exception {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(planetDataSource());
        factoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mapper/planet/*.xml"));
        return factoryBean.getObject();
    }

    @Bean
    public SqlSessionTemplate planetSqlSessionTemplate() throws Exception {
        return new SqlSessionTemplate(planetSqlSessionFactory());
    }
}
