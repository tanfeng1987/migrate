/****************************************************************************************
 南京小视科技有限公司
 ****************************************************************************************/
package com.minivision.migrate.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <Description><br>
 *
 * @author luyaoqiang<br>
 * @version 1.2<br>
 * @taskId <br>
 * @CreateDate 2020/9/29 <br>
 */
@Data
public class PlanetPersonDTO implements Serializable {
    private static final long serialVersionUID = -4683305335123148265L;

    /**
     * id
     */
    private String id;

    /**
     * 所属客户id(企业id)
     */
    private String companyId;

    /**
     * 名称
     */
    private String name;

    /**
     * 人员类型(没指定时为员工)
     */
    private Byte type;

    /**
     * 工号/编号 可为空
     */
    private String no;

    /**
     * 性别
     */
    private Byte sex;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 年龄
     */
    private Byte age;

    /**
     * 身份证id
     */
    private String cardNo;

    /**
     * 展示照片(用于展示)
     */
    private String showPhoto;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;



    /**
     * IC卡号
     */
    private String icCard;

    private Integer communityId;
    private Integer orgId;
}
