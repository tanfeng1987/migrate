/****************************************************************************************
 南京小视科技有限公司
 ****************************************************************************************/
package com.minivision.migrate.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <Description><br>
 *
 * @author luyaoqiang<br>
 * @version 1.2<br>
 * @taskId <br>
 * @CreateDate 2020/9/29 <br>
 */
@Data
public class PlanetDeviceDTO implements Serializable {
    private static final long serialVersionUID = -7910839473697783412L;

    /**
     * 唯一标识
     */
    private String id;

    /**
     * 应用key
     */
    private String appKey;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 设备编码
     */
    private String deviceCode;

    /**
     * 设备描述
     */
    private String deviceDesc;

    /**
     * 设备类型
     */
    private String deviceType;

    /**
     * 当前版本
     */
    private String version;

    private String deviceIpv4;

    private String deviceMac;

    /**
     * 上级设备
     */
    private String pId;

    /**
     * 是否删除0未删除1已删除
     */
    private String isDelete;

    /**
     * 创建时间
     */
    private Date createTime;

    private String creator;

    /**
     * 创建人ID
     */
    private String creatorId;

    /**
     * 修改时间
     */
    private Date updateTime;

    private String modifier;

    /**
     * 修改人ID
     */
    private String modifierId;

    /**
     * 上级设备编码
     */
    private String pCode;

    /**
     * 设备型号
     */
    private String specification;

    /**
     * 位置名称
     */
    private String regionName;

    /**
     * 经度
     */
    private Double longitude;

    /**
     * 纬度
     */
    private Double latitude;

    /**
     * 设备是否在线，0-不在线，1-在线
     */
    private String status;

    /**
     * 星环云中，0入口，1出口
     */
    private String inOutType;

    /**
     * 设备状态， 1:离线 0:在线
     */
    private Integer deviceStatus;

    /**
     * 设备状态变更时间
     */
    private Date deviceStatusUpdateTime;

}
