
package com.minivision.migrate.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.util.StringUtil;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.SecureRandom;

/**
 * <Description> <br>
 * 
 * @author shaokangwei<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2018年1月12日 <br>
 */
@Component
public class EncryptUtil {

    /**
     * 日志记录器
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(EncryptUtil.class);

    /**
     * 用于加密的根据 key
     */
    private static String defaultKey;

    /**
     * AES字符
     */
    private static final String AES = "AES";

    /**
     * 初始化key
     */
    private static final int INITKEYNUM = 128;

    /**
     * 8位数的字节数组
     */
    private static final int ARRLENGTH = 8;

    /**
     * 相加数
     */
    private static final int ADDNUM = 256;

    /**
     * 比较数
     */
    private static final int COMPARENUM = 16;

    /**
     * encryptCipher
     */
    private Cipher encryptCipher = null;

    /**
     * decryptCipher
     */
    private Cipher decryptCipher = null;

    static {
//        defaultKey = new ProperiesUtil("encrypt.properties").getProperty("aes.key");
        // 获取key为空，默认key
        if (null == defaultKey) {
            defaultKey = "minivision123";
        }

    }

    /**
     * 构造
     */
    public EncryptUtil() {
        this(defaultKey);
    }

    /**
     * 构造
     *
     * @param key <br>
     */
    public EncryptUtil(String key) {
        KeyGenerator kgen;
        try {
            kgen = KeyGenerator.getInstance(AES);
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
            // 实现各个操作操作系统产生相同的key
            secureRandom.setSeed(key.getBytes());
            kgen.init(INITKEYNUM, secureRandom);

            SecretKey secretKey = kgen.generateKey();
            byte[] enCodeFormat = secretKey.getEncoded();
            SecretKeySpec keyTmp = new SecretKeySpec(enCodeFormat, AES);

            encryptCipher = Cipher.getInstance(AES);
            encryptCipher.init(Cipher.ENCRYPT_MODE, keyTmp);

            decryptCipher = Cipher.getInstance(AES);
            decryptCipher.init(Cipher.DECRYPT_MODE, keyTmp);
        } catch (Exception e) {
            LOGGER.error("some error when init AESUtil {}.", e);
        }
    }

    /**
     * Description: 从指定字符串生成密钥，密钥所需的字节数组长度为8位 不足8位时后面补0，超出8位只取前8位 <br>
     *
     * @author qss<br>
     * @taskId <br>
     * @param bytes 构成该字符串的字节数组<br>
     * @return 生成的密钥<br>
     */
    public Key getKey(byte[] bytes) {
        // 创建一个空的8位字节数组（默认值为0）
        byte[] arrB = new byte[ARRLENGTH];

        // 将原始字节数组转换为8位
        for (int i = 0; i < bytes.length && i < arrB.length; i++) {
            System.arraycopy(bytes, i, arrB, i, 1);
        }

        // 生成密钥
        return new SecretKeySpec(arrB, AES);
    }

    /**
     * Description:加密字节数组 <br>
     *
     * @author qss<br>
     * @taskId <br>
     * @param bytes 需加密的字节数组<br>
     * @return 加密后的字节数组<br>
     */
    private byte[] encrypt(byte[] bytes) {
        byte[] result = null;
        try {
            result = encryptCipher.doFinal(bytes);
        } catch (Exception e) {
            LOGGER.warn("some error when encrypt", e);
        }
        return result;
    }

    /**
     * Description: 解密字节数组<br>
     *
     * @author qss<br>
     * @taskId <br>
     * @param bytes 需解密的字节数组<br>
     * @return 解密后的字节数组 <br>
     */
    private byte[] decrypt(byte[] bytes) {
        byte[] result = null;
        try {
            result = decryptCipher.doFinal(bytes);
        } catch (Exception e) {
            LOGGER.warn("some error when decrypt", e);
        }

        return result;
    }

    /**
     * Description: 将byte数组转换为表示16进制值的字符串， 如：byte[]{8,18}转换为：0813， 和public static byte[] hexStr2ByteArr(String strIn) 互为可逆的转换过程 <br>
     *
     * @author qss<br>
     * @taskId <br>
     * @param bytes 需要转换的byte数组<br>
     * @return 转换后的字符串<br>
     */
    private static String byteArr2HexStr(byte[] bytes) {
        int iLen = bytes.length;
        // 每个byte用两个字符才能表示，所以字符串的长度是数组长度的两倍
        StringBuilder sb = new StringBuilder(iLen * 2);
        for (int i = 0; i < iLen; i++) {
            int intTmp = bytes[i];
            // 把负数转换为正数
            while (intTmp < 0) {
                intTmp = intTmp + ADDNUM;
            }
            // 小于0F的数需要在前面补0
            if (intTmp < COMPARENUM) {
                sb.append('0');
            }
            sb.append(Integer.toString(intTmp, COMPARENUM));
        }
        return sb.toString();
    }

    /**
     * Description: 将表示16进制值的字符串转换为byte数组.互为可逆的转换过程<br>
     *
     * @author qss<br>
     * @taskId <br>
     * @param text 需要转换的字符串<br>
     * @return 转换后的byte数组<br>
     */
    private byte[] hexStr2ByteArr(String text) {
        byte[] arrB = text.getBytes(StandardCharsets.UTF_8);
        int iLen = arrB.length;

        // 两个字符表示一个字节，所以字节数组长度是字符串长度除以2
        byte[] arrOut = new byte[iLen / 2];
        for (int i = 0; i < iLen; i = i + 2) {
            String strTmp = new String(arrB, i, 2);
            arrOut[i / 2] = (byte) Integer.parseInt(strTmp, COMPARENUM);
        }
        return arrOut;
    }

    /**
     * Description: 加密字符串<br>
     *
     * @author qss<br>
     * @taskId <br>
     * @param content <br>
     * @return <br>
     */
    public static String encrypt(String content) {
        if (StringUtil.isEmpty(content)) {
            return content;
        }

        try {
            EncryptUtil aes = new EncryptUtil();
            return byteArr2HexStr(aes.encrypt(content.getBytes(StandardCharsets.UTF_8)));
        } catch (Exception e) {
            LOGGER.error("some error when encrypt string {}.", e);
        }
        return null;
    }

    /**
     * Description: 解密字符串 <br>
     *
     * @author qss<br>
     * @taskId <br>
     * @param content 需解密的字符串<br>
     * @return <br>
     */
    public static String decrypt(String content) {
        if (StringUtil.isEmpty(content)) {
            return content;
        }

        byte[] bytes = null;
        try {
            EncryptUtil aes = new EncryptUtil();
            bytes = aes.decrypt(aes.hexStr2ByteArr(content));
        } catch (Exception e) {
            LOGGER.warn("some error when decrypt string {}", content, e);
        }

        if (null != bytes) {
            try {
                // 这里加个utf8编码 不然会乱码 其他是好的 为什么呢 maven打包编码突然变成GBK了
                return new String(bytes, StandardCharsets.UTF_8);
            } catch (Exception e) {
                return new String(bytes);
            }
        } else {
            return content;
        }
    }

    /**
     * 解密字节数组 Description: <br>
     *
     * @author zhangshuxing<br>
     * @taskId <br>
     * @param content 字节数组
     * @return <br>
     */
    public static byte[] decryptBytes(byte[] content) {
        byte[] bytes = null;
        try {
            EncryptUtil aes = new EncryptUtil();
            bytes = aes.decrypt(content);
        } catch (Exception e) {
            LOGGER.error("some error when decrypt string . {}.", e);
        }
        return bytes;
    }

    /**
     * Description: 加密字节数组<br>
     *
     * @author zhangshuxing<br>
     * @taskId <br>
     * @param content <br>
     * @return <br>
     */
    public static byte[] encryptBytes(byte[] content) {
        try {
            EncryptUtil aes = new EncryptUtil();
            return aes.encrypt(content);
        } catch (Exception e) {
            LOGGER.error("some error when encrypt string {}.", e);
        }
        return new byte[0];
    }

}
