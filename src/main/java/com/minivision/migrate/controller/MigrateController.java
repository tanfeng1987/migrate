package com.minivision.migrate.controller;

import com.minivision.migrate.service.DataPlanetService;
import com.minivision.migrate.service.MigrateService;
import com.minivision.migrate.service.PlanetService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@Slf4j
public class MigrateController {

    @Autowired
    private MigrateService migrateService;

    @Autowired
    private PlanetService planetService;

    @Autowired
    private DataPlanetService dataPlanetService;

    @PostMapping("/migrate")
    public String migrate(@RequestBody HashMap<String, Integer> map) {
        int communityId = map.get("orgId");
        // 先处理小区和房屋信息
        Integer villageId = migrateService.migrateVillageAndHouse(communityId);

        // 处理设备信息
        planetService.dealDevice(String.valueOf(communityId));

        // 再处理人员信息
        planetService.dealPlanetPersonByCommunityId(String.valueOf(communityId));

        // 处理识别记录
        migrateService.migratePadRecord(villageId);
        return "success";
    }

     /**
     * 处理人员
     * @param map
     */
     @PostMapping("/dealPlanetPersonByCommunityId")
     public String dealPlanetPersonByCommunityId(@RequestBody HashMap<String, String> map) {
         planetService.dealPlanetPersonByCommunityId(map.get("communityId"));
         return "success";
     }

    /**
     * 处理设备
     * @param map
     */
    @PostMapping("/dealDevice")
    public String dealDevice(@RequestBody HashMap<String, String> map) {
        planetService.dealDevice(map.get("communityId"));
        return "success";
    }

    /**
     * 清洗星环云数据到数据中心
     *  客户账户登录后，在组织管理中的小区id（也就是客户账户登录后第一级组织的id）
     */
    @Scheduled(cron = "0 0 1 * * *")
    @PostMapping("/sync")
    public String sync() {

        log.info("定时器执行开始");
        dataPlanetService.sync();
        log.info("定时器执行结束");

        return "success";
    }
}
