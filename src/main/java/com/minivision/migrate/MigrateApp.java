/**************************************************************************************** 
 南京小视科技有限公司           
 ****************************************************************************************/
package com.minivision.migrate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.RestController;

/**
 * <Description> <br>
 *
 * @author qss<br>
 * @version 1.0<br>
 * @taskId <br>
 * @CreateDate 2017年7月7日 <br>
 */
@SpringBootApplication
@RestController
@EnableScheduling
@ComponentScan(basePackages = {"com.minivision"})
public class MigrateApp {

    /**
     * Description: 启动springboot<br>
     *
     * @param args <br>
     * @author QSS<br>
     * @taskId <br>
     */
    public static void main(String[] args) {
        ConfigurableApplicationContext confApp = null;
        try {
            confApp = SpringApplication.run(MigrateApp.class, args);
        } finally {
            close(confApp);
        }
    }

    private static void close(ConfigurableApplicationContext confApp) {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                if (confApp != null) {
                    confApp.close();
                }
            }
        });
    }
}
